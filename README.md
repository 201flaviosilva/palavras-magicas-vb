# Palavras Magicas

## Description

Uma simples brincadeira com palavras, tal como juntar 5 palavras em uma só, todas em letras em MAIÚSCULAS ou <sub>minusculas</sub>, etc...

## Preview

![Preview](https://bitbucket.org/201flaviosilva/palavras-magicas-vb/downloads/Preview.gif)
